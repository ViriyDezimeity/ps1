import express from "express";
import db from './db';
// создаем объект приложения
const app = express();
// определяем обработчик для маршрута "/users"
app.get("/users", async function(request, response){
    const res = { 
      users: (await db.query('SELECT * FROM users')).rows
    }
    // отправляем ответ
    response.send(res);
});
// определяем обработчик для маршрута "/projects"
app.get("/projects", async function(request, response){
  const res = { 
    projects: (await db.query('SELECT * FROM projects')).rows
  }
  // отправляем ответ
  response.send(res);
});

// начинаем прослушивать подключения на 3000 порту
app.listen(3000);
